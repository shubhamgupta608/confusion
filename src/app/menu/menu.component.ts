import { Component, OnInit, Inject } from '@angular/core';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
// import { DISHES } from '../shared/dishes'
import { expand,flyInOut } from '../animations/app.animation'
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  host:{
    '[@flyInOut]': 'true',
    'style':'display:block;'
  },
  animations:[
    flyInOut(),
    expand()
  ]
})
export class MenuComponent implements OnInit {


  // dishes:Dish[] = DISHES;
  dishes:Dish[];
  errMess:string;
  // selectedDish: Dish; 

  constructor(private _dishService:DishService,
    @Inject('BaseURL') private BaseURL) { }

  ngOnInit() {
    this.getDishes();
  }


  // getDishes(){
  //     this._dishService.getDishes()
  //   .then((dishes)=> this.dishes = dishes);
  // }
  getDishes(){
    this._dishService.getDishes().subscribe((dishes)=>
     this.dishes = dishes,
     (errmess)=>this.errMess = <any>errmess);
}
  // onSelect(dish:Dish){
  //   this.selectedDish = dish;
  // }

}
