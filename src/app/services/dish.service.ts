import { Injectable } from '@angular/core';
import { Dish } from '../shared/dish';
// import { DISHES } from '../shared/dishes';
import { Observable, of } from 'rxjs'; //
import { delay, map,catchError } from 'rxjs/operators'; //provide delay the emiting of the items from the observable
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHTTPMsgService } from './process-httpmsg.service';
@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor(private http:HttpClient,
    private processHTTPMsgService:ProcessHTTPMsgService) { }

  // getDishes():Promise<Dish[]>{
  //  return Promise.resolve(DISHES);
  // }

  // getDishes():Promise<Dish[]>{
  //  return new Promise((resolve,reject)=>{
  //   //Simulate server latency with 2 seconds delay
  //   setTimeout(()=>resolve(DISHES),2000) 
  //  });
  // }

  // getDishes():Promise<Dish[]>{
  //   return of(DISHES).pipe(delay(2000)).toPromise(); 
  //  }


  // getDishes(): Observable<Dish[]> {
  //   return of(DISHES).pipe(delay(2000))
  // }

  getDishes(): Observable<Dish[]> {
    return this.http.get<Dish[]>(baseURL + 'dishes')
    .pipe(catchError(this.processHTTPMsgService.handleError))
  }



  getDish(id: string): Observable<Dish> {
    return this.http.get<Dish>(baseURL + 'dishes/' + id)
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getFeaturedDish(): Observable<Dish> {
    return this.http.get<Dish[]>(baseURL + 'dishes?featured=true')
    .pipe(map(dishes=>dishes[0]))
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getDishIds(): Observable<string[] | any> {
    return this.getDishes().pipe(map(dishes=> dishes.map(dish=>dish.id)))
    .pipe(catchError(error => error))
  }

  putDish(dish:Dish):Observable<Dish> {
    const httpOptions = {
        headers:new HttpHeaders({
          'Content-Type':'application/json'
        })
    };
    return this.http.put<Dish>(baseURL + 'dishes/' + dish.id, dish, httpOptions)
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }
}
