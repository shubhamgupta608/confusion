import { Injectable } from '@angular/core';
import { Promotion } from '../shared/promotion';
import { PROMOTIONS } from '../shared/promotions';
import { delay, catchError, map } from 'rxjs/operators';
import { of, Observable, ObservableLike } from 'rxjs';
import { baseURL } from '../shared/baseurl';
import { ProcessHTTPMsgService } from './process-httpmsg.service';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  constructor(private processHTTPMsgService:ProcessHTTPMsgService,
    private http:HttpClient) { }


  getPromotions():Observable<Promotion[]>{
    return this.http.get<Promotion[]>(baseURL + 'promotions') 
    .pipe(catchError(this.processHTTPMsgService.handleError))
    
   }
 
   getPromotion(id:string):Observable<Promotion>{
     return this.http.get<Promotion>(baseURL + 'promotions/' + id)
    .pipe(catchError(this.processHTTPMsgService.handleError))
   }
 
   getFeaturedPromotion(): Observable<Promotion> {
    return  this.http.get<Promotion[]>(baseURL + 'promotions?featured=true')
    .pipe(map(promotions=>promotions[0]))
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  //  getFeaturedPromotion(): Observable<Promotion> {
  //   return of(PROMOTIONS.filter((promotion) => promotion.featured)[0]).pipe(delay(2000));
  // }
}
